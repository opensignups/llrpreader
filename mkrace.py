#!/usr/bin/env python

import csv


newdata = []

with open('tt2013.csv', 'r') as f:
    #bib,first,last,city,state,age,sex,start,finish
    rdr = csv.reader(f)
    for row in rdr:
        r = list(row)
        newdata.append(r)
        for i in [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000]:
            newr = list(r)
            newr[0] = str(i + int(r[0]))
            newr[7] = str(i + int(r[7]))
            newr[8] = str(10*i + int(r[8]))
            newdata.append(newr)

for r in newdata:
    print r
        
with open('tt2013x.csv', 'wb') as csvfile:
    wrt = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
    for r in newdata:
        wrt.writerow(r)
