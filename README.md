# README #


**llrpreader** is java application used to emulate an Impinj Raceway R420 UHF RFID reader at the network level.  It supports telnet and LLRP protocols.  It is not intended
to emulate every aspect of a reader, just enough to support the [racemanager](https://bitbucket.org/opensignups/racemanager) race application.

### To use: ###

1. Clone repository
1. mvn package assembly:single
1. java -jar target/llrpreader-<version>-with-dependencies.jar [--port=<llrp port>] [--telnetPort=<telnet port>] [--reportFile=<report csv>]

###### Command Line Parameters ######
* *--port* is the port the LLRP engine listens to, defaults to 5084
* *--telnetPort* is the port the telnet server listens to, defaults to 2323
* *--reportFile* is a file containing bibs, start and finish times, defaults to none

### Current Build Status ###
[![Build Status](https://api.shippable.com/projects/54ad902bd46935d5fbc1b94b/badge?branchName=master)](https://app.shippable.com/projects/54ad902bd46935d5fbc1b94b/builds/latest)