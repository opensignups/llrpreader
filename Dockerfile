FROM openjdk:8
MAINTAINER dsr

RUN apt-get update -y && apt-get install -y maven git curl vim
EXPOSE 5084 23 22

WORKDIR /app
COPY src/ /app/src/
COPY pom.xml /app/
RUN mvn clean package
COPY tt2013.csv /app/
CMD java -jar target/llrpreader-0.0.1-SNAPSHOT.jar --reportfile=$REPORT --telnetPort=23 --sshPort=22
