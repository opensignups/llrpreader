package llrpreader;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.llrp.ltk.generated.enumerations.GetReaderCapabilitiesRequestedData;
import org.llrp.ltk.generated.enumerations.GetReaderConfigRequestedData;
import org.llrp.ltk.generated.messages.DELETE_ACCESSSPEC;
import org.llrp.ltk.generated.messages.DELETE_ACCESSSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.DELETE_ROSPEC;
import org.llrp.ltk.generated.messages.DELETE_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.GET_READER_CAPABILITIES;
import org.llrp.ltk.generated.messages.GET_READER_CAPABILITIES_RESPONSE;
import org.llrp.ltk.generated.messages.GET_READER_CONFIG;
import org.llrp.ltk.generated.messages.GET_READER_CONFIG_RESPONSE;
import org.llrp.ltk.generated.parameters.AntennaProperties;
import org.llrp.ltk.generated.parameters.GeneralDeviceCapabilities;
import org.llrp.ltk.generated.parameters.Identification;
import org.llrp.ltk.net.LLRPConnection;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;
import org.llrp.ltk.net.LLRPConnector;
import org.llrp.ltk.net.LLRPEndpoint;
import org.llrp.ltk.types.LLRPMessage;
import org.llrp.ltk.types.UnsignedInteger;
import org.llrp.ltk.types.UnsignedShort;

import com.opensignups.timing.llrpreader.LlrpReader;

import expect4j.Closure;
import expect4j.Expect4j;
import expect4j.ExpectState;
import expect4j.ExpectUtils;

public class LlrpReaderTest {

	public static final String LOCALHOST = "localhost";
    public static final UnsignedShort USZERO = new UnsignedShort(0);
    public static final UnsignedInteger UIZERO = new UnsignedInteger(0);

    LlrpReader reader = new LlrpReader();
    LLRPConnection connection; 
            
	@Before
	public void setUp() throws IOException, LLRPConnectionAttemptFailedException {
       reader = new LlrpReader();
       reader.bind();
       
       LLRPEndpoint endpoint = new LLRPEndpoint() {
    
            @Override
            public void messageReceived(LLRPMessage message) {
                // TODO Auto-generated method stub
                
            }
    
            @Override
            public void errorOccured(String message) {
                // TODO Auto-generated method stub
                
            };
        };
        connection = new LLRPConnector(endpoint, LOCALHOST, reader.getPort());
        ((LLRPConnector) connection).connect();
	}
	
	@After
	public void tearDown() {
        connection = null;
        reader.stop();
        reader = null;
	}
	
	class AsyncResult {
		
		protected static final int DEFAULT_RETRIES = 10;
		protected static final int DEFAULT_MILLIS = 100;
		
		protected final int retries;
		protected final int millis;
		
		protected boolean result = false;
		
		public AsyncResult() {
			this.retries = DEFAULT_RETRIES;
			this.millis = DEFAULT_MILLIS;
			result = false;
		}
		
		public AsyncResult(int retries, int millis) {
			this.retries = retries;
			this.millis = millis;
			this.result = false;
		}
		
		boolean isSuccess() {
			int retry = this.retries;
			while (retry-- > 0) {
				if (result)
					break;
				try {
					Thread.sleep(100);
				} catch (InterruptedException ex) {
					// noop
				}
			}
			return result;
		}
		
		void setResult(boolean result) {
			this.result = result;
		}
		
		public Closure closure() {
			final AsyncResult that = this;
			return new Closure() {
				@Override
				public void run(ExpectState state) throws Exception {
					that.setResult(true);
				}
			};
		}
	}
	
	@Test
	public void testTelnetSetTime() throws Exception {
		final Expect4j e = ExpectUtils.telnet(LOCALHOST, reader.getTelnetPort());

		final AsyncResult result = new AsyncResult();
		
		e.expect("login:");
		e.send("root\r\n");
		e.expect("Password:");
		e.send("impinj\r\n");
		e.expect(">");
		DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		String time = formatter.format(new Date());
		e.send("config system time " + time + "\r\n");
		// config system time ccyy.mm.dd-hh:mm:ss
		e.expect("Status='0,Success'", result.closure());

		assertTrue("Expected success from config set time", result.isSuccess());
		
		e.send("exit\r\n");
		e.close();
	}

	@Test
	public void testTelnetShowSummary() throws Exception {
		final Expect4j e = ExpectUtils.telnet(LOCALHOST, reader.getTelnetPort());

		final AsyncResult result = new AsyncResult();
		
		e.expect("login:");
		e.send("root\r\n");
		e.expect("Password:");
		e.send("impinj\r\n");
		e.expect(">");
		e.send("show system summary\r\n");
		e.expect(">", new Closure() {

			@Override
			public void run(ExpectState state) throws Exception {
				final String buffer = state.getBuffer();
				boolean ok = buffer.contains("Status='0,Success'");
				ok = ok && buffer.contains("SysDesc=");
				ok = ok && buffer.contains("SerialNumber=");
				ok = ok && buffer.contains("MACAddress=");
				
				result.setResult(ok);
			}
		});

		assertTrue("Expected SysDesc, SerialNumber, MACAddress in buffer", result.isSuccess());
		
		e.send("exit\r\n");
		e.close();
	}

	@Test
	public void testTelnetShowPlatform() throws Exception {
		final Expect4j e = ExpectUtils.telnet(LOCALHOST, reader.getTelnetPort());

		final AsyncResult result = new AsyncResult();
		
		e.expect("login:");
		e.send("root\r\n");
		e.expect("Password:");
		e.send("impinj\r\n");
		e.expect(">");
		e.send("show system platform\r\n");
		e.expect(">", new Closure() {

			@Override
			public void run(ExpectState state) throws Exception {
				final String buffer = state.getBuffer();
				boolean ok = buffer.contains("Status='0,Success'");
				ok = ok && buffer.contains("BootEnvVersion=");
				ok = ok && buffer.contains("HLAVersion=");
				ok = ok && buffer.contains("HardwareVersion=");
				ok = ok && buffer.contains("IntHardwareVersion=");
				ok = ok && buffer.contains("SerialNumber=");
				ok = ok && buffer.contains("IntSerialNumber=");
				ok = ok && buffer.contains("MACAddress=");
				ok = ok && buffer.contains("FeaturesValid=");
				ok = ok && buffer.contains("BIOSVersion=");
				ok = ok && buffer.contains("PTN=");
				ok = ok && buffer.contains("UptimeSeconds=");
				ok = ok && buffer.contains("BootStatus=");
				ok = ok && buffer.contains("BootReason=");
				ok = ok && buffer.contains("PowerFailTime=");
				ok = ok && buffer.contains("ActivePowerSource=");

				result.setResult(ok);
			}
		});

		assertTrue("Expected SysDesc, SerialNumber, MACAddress in buffer", result.isSuccess());
		
		e.send("exit\r\n");
		e.close();
	}
	   
    @Test
    public void testSshSetTime() throws Exception {
        final Expect4j e = ExpectUtils.SSH(LOCALHOST, "root", "impinj", reader.getSshPort());

        final AsyncResult result = new AsyncResult();
        
        e.expect(">");
        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        String time = formatter.format(new Date());
        e.send("config system time " + time + "\r\n");
        // config system time ccyy.mm.dd-hh:mm:ss
        e.expect("Status='0,Success'", result.closure());

        assertTrue("Expected success from config set time", result.isSuccess());
        
        e.send("exit\r\n");
        e.close();
    }

    @Test
    public void testSshShowPlatform() throws Exception {
        final Expect4j e = ExpectUtils.SSH(LOCALHOST, "root", "impinj", reader.getSshPort());

        final AsyncResult result = new AsyncResult();
        
        e.expect(">");
        e.send("show system platform\r\n");
        e.expect(">", new Closure() {

            @Override
            public void run(ExpectState state) throws Exception {
                final String buffer = state.getBuffer();
                boolean ok = buffer.contains("Status='0,Success'");
                ok = ok && buffer.contains("BootEnvVersion=");
                ok = ok && buffer.contains("HLAVersion=");
                ok = ok && buffer.contains("HardwareVersion=");
                ok = ok && buffer.contains("IntHardwareVersion=");
                ok = ok && buffer.contains("SerialNumber=");
                ok = ok && buffer.contains("IntSerialNumber=");
                ok = ok && buffer.contains("MACAddress=");
                ok = ok && buffer.contains("FeaturesValid=");
                ok = ok && buffer.contains("BIOSVersion=");
                ok = ok && buffer.contains("PTN=");
                ok = ok && buffer.contains("UptimeSeconds=");
                ok = ok && buffer.contains("BootStatus=");
                ok = ok && buffer.contains("BootReason=");
                ok = ok && buffer.contains("PowerFailTime=");
                ok = ok && buffer.contains("ActivePowerSource=");

                result.setResult(ok);
            }
        });

        assertTrue("Expected SysDesc, SerialNumber, MACAddress in buffer", result.isSuccess());
        
        e.send("exit\r\n");
        e.close();
    }
	@Test
	public void testSshShowSummary() throws Exception {
		final Expect4j e = ExpectUtils.SSH(LOCALHOST, "root", "impinj", reader.getSshPort());

		final AsyncResult result = new AsyncResult();
		
		e.expect(">");
		e.send("show system summary\r\n");
		e.expect(">", new Closure() {

			@Override
			public void run(ExpectState state) throws Exception {
				final String buffer = state.getBuffer();
				boolean ok = buffer.contains("Status='0,Success'");
				ok = ok && buffer.contains("SysDesc=");
				ok = ok && buffer.contains("SerialNumber=");
				ok = ok && buffer.contains("MACAddress=");
				
				result.setResult(ok);
			}
		});

		assertTrue("Expected SysDesc, SerialNumber, MACAddress in buffer", result.isSuccess());
		
		e.send("exit\r\n");
		e.close();
	}

	@Test
	public void testDeleteAccessSpec() throws TimeoutException {
	    final LLRPMessage msg = new DELETE_ACCESSSPEC();
        ((DELETE_ACCESSSPEC) msg).setAccessSpecID(UIZERO);
	    final LLRPMessage response = connection.transact(msg);
	    
	    assertTrue("Response expected to be DELETE_ACCESSSPEC_RESPONSE", response instanceof DELETE_ACCESSSPEC_RESPONSE);
	}

    @Test
    public void testDeleteRoSpec() throws TimeoutException {
        final LLRPMessage msg = new DELETE_ROSPEC();
        ((DELETE_ROSPEC) msg).setROSpecID(UIZERO);
        final LLRPMessage response = connection.transact(msg);
        
        assertTrue("Response expected to be DELETE_ROSPEC_RESPONSE", response instanceof DELETE_ROSPEC_RESPONSE);
    }

    @Test
    public void testGetReaderCapabilities() throws TimeoutException {
        GetReaderCapabilitiesRequestedData requestedData = new GetReaderCapabilitiesRequestedData();
        requestedData.set("All");
        final LLRPMessage msg = new GET_READER_CAPABILITIES();
        ((GET_READER_CAPABILITIES) msg).setRequestedData(requestedData);
        final LLRPMessage response = connection.transact(msg);
        
        assertTrue("Response expected to be GET_READER_CAPABILITIES_RESPONSE", response instanceof GET_READER_CAPABILITIES_RESPONSE);
        GeneralDeviceCapabilities caps = ((GET_READER_CAPABILITIES_RESPONSE) response).getGeneralDeviceCapabilities();
        
        assertTrue("Expected at least one antenna", caps.getMaxNumberOfAntennaSupported().intValue() > 0);
    }

    @Test
    public void testGetReaderConfig() throws TimeoutException {
        final GET_READER_CONFIG msg = new GET_READER_CONFIG();
        GetReaderConfigRequestedData requestedData = new GetReaderConfigRequestedData();
        msg.setAntennaID(USZERO);
        msg.setGPIPortNum(USZERO);
        msg.setGPOPortNum(USZERO);
        msg.setRequestedData(requestedData);

        final LLRPMessage response = connection.transact(msg);
        
        assertTrue("Response expected to be GET_READER_CONFIG_RESPONSE", response instanceof GET_READER_CONFIG_RESPONSE);
        
        GET_READER_CONFIG_RESPONSE config = (GET_READER_CONFIG_RESPONSE) response;
        
        final Identification id = config.getIdentification();
        assertNotNull("Expected non-null id", config.getIdentification());
        final String readerId = id.getReaderID().toString();
        assertNotNull("Expected non-null readerId", readerId);

        //final List<AntennaConfiguration> antennaConfigs = config.getAntennaConfigurationList();
        //assertFalse("Expected at least one antenna config", antennaConfigs.isEmpty());
        final List<AntennaProperties> antennaProperties = config.getAntennaPropertiesList();
        assertFalse("Expected at least one antenna property", antennaProperties.isEmpty());
    }
}
