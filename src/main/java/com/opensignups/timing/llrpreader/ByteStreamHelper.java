package com.opensignups.timing.llrpreader;

import java.io.DataInputStream;
import java.io.IOException;

import org.llrp.ltk.types.LLRPBitList;
import org.llrp.ltk.types.UnsignedInteger;

public class ByteStreamHelper extends LlrpReader {

    private final DataInputStream in;
    private byte[] data;
    private boolean eof = false;
    private boolean debug = false;

    public ByteStreamHelper(final DataInputStream in) {
        this.in = in;
    }
    
    public boolean isEof() {
        return eof;
    }
    
    public byte[] getBytes() throws IOException {
        byte[] tempBuffer = null;
        byte[] returnBuffer = null;
        
        int bufferAvailable = data == null ? 0 : data.length;
        if (bufferAvailable < 6) {
            int streamAvailable = in.available();
            if (streamAvailable > 0) {
                byte[] bytes = new byte[streamAvailable];
                if (debug) {
                    System.out.println("reading bytes from stream: " + streamAvailable);
                }
                in.read(bytes);
                tempBuffer = concat(data, bytes);
                data = tempBuffer;
            }
        }
        
        int len = calculateLength(data);
        if (len > 0) {
            returnBuffer = new byte[len];
            System.arraycopy(data, 0, returnBuffer, 0, len);
            tempBuffer = new byte[data.length - len];
            System.arraycopy(data, len, tempBuffer, 0, data.length - len);
            data = tempBuffer;
        }

        if (debug && returnBuffer != null && returnBuffer.length > 0) {
            System.out.println("return buffer len: " + ((returnBuffer == null) ? 0 : returnBuffer.length));
            System.out.println("data buffer len: " + ((data == null) ? 0 : data.length));
        }
        
        return returnBuffer;
    }
    
    private int calculateLength(byte[] bytes) {
        if (bytes == null || bytes.length < 6) {
            return 0;
        }
        
        if (debug) {
            for (int i = 0; i < bytes.length; i++) {
                String s = Integer.toHexString(bytes[i]);
                if (s.length() < 2) {
                    s = "0" + s;
                } else if (s.length() > 2) {
                    s.substring(0,  2);
                }
                System.out.print(s + " ");
            }
            System.out.println();
        }
        LLRPBitList bits = new LLRPBitList(bytes);
        UnsignedInteger messageLength = new UnsignedInteger(bits.subList(16,
                UnsignedInteger.length()));

        return messageLength.intValue();
    }

    public byte[] concat(byte[] a, byte[] b) {
        if (a == null && b != null) {
            return b;
        }
        if (a != null && b == null) {
            return a;
        }
        if (a == null && b == null) {
            return a;
        }
        int aLen = a.length;
        int bLen = b.length;
        byte[] c= new byte[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
     }
}
