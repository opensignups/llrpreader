package com.opensignups.timing.llrpreader;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

import org.apache.sshd.common.Factory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.password.PasswordAuthenticator;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.session.ServerSession;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.llrp.ltk.exceptions.InvalidLLRPMessageException;
import org.llrp.ltk.generated.LLRPMessageFactory;
import org.llrp.ltk.generated.enumerations.ConnectionAttemptStatusType;
import org.llrp.ltk.generated.enumerations.IdentificationType;
import org.llrp.ltk.generated.enumerations.StatusCode;
import org.llrp.ltk.generated.messages.ADD_ROSPEC;
import org.llrp.ltk.generated.messages.ADD_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.CLOSE_CONNECTION;
import org.llrp.ltk.generated.messages.CLOSE_CONNECTION_RESPONSE;
import org.llrp.ltk.generated.messages.DELETE_ACCESSSPEC;
import org.llrp.ltk.generated.messages.DELETE_ACCESSSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.DELETE_ROSPEC;
import org.llrp.ltk.generated.messages.DELETE_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.ENABLE_ROSPEC;
import org.llrp.ltk.generated.messages.ENABLE_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.GET_READER_CAPABILITIES;
import org.llrp.ltk.generated.messages.GET_READER_CAPABILITIES_RESPONSE;
import org.llrp.ltk.generated.messages.GET_READER_CONFIG;
import org.llrp.ltk.generated.messages.GET_READER_CONFIG_RESPONSE;
import org.llrp.ltk.generated.messages.GET_REPORT;
import org.llrp.ltk.generated.messages.KEEPALIVE;
import org.llrp.ltk.generated.messages.READER_EVENT_NOTIFICATION;
import org.llrp.ltk.generated.messages.RO_ACCESS_REPORT;
import org.llrp.ltk.generated.messages.SET_READER_CONFIG;
import org.llrp.ltk.generated.messages.SET_READER_CONFIG_RESPONSE;
import org.llrp.ltk.generated.messages.START_ROSPEC;
import org.llrp.ltk.generated.messages.START_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.STOP_ROSPEC;
import org.llrp.ltk.generated.messages.STOP_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.parameters.AntennaID;
import org.llrp.ltk.generated.parameters.AntennaProperties;
import org.llrp.ltk.generated.parameters.ConnectionAttemptEvent;
import org.llrp.ltk.generated.parameters.EPCData;
import org.llrp.ltk.generated.parameters.FirstSeenTimestampUTC;
import org.llrp.ltk.generated.parameters.Identification;
import org.llrp.ltk.generated.parameters.LLRPStatus;
import org.llrp.ltk.generated.parameters.LastSeenTimestampUTC;
import org.llrp.ltk.generated.parameters.PeakRSSI;
import org.llrp.ltk.generated.parameters.ReaderEventNotificationData;
import org.llrp.ltk.generated.parameters.TagReportData;
import org.llrp.ltk.generated.parameters.UTCTimestamp;
import org.llrp.ltk.net.LLRPConnection;
import org.llrp.ltk.types.Bit;
import org.llrp.ltk.types.BitArray_HEX;
import org.llrp.ltk.types.BitList;
import org.llrp.ltk.types.LLRPMessage;
import org.llrp.ltk.types.SignedByte;
import org.llrp.ltk.types.SignedShort;
import org.llrp.ltk.types.UTF8String_UTF_8;
import org.llrp.ltk.types.UnsignedByteArray_HEX;
import org.llrp.ltk.types.UnsignedInteger;
import org.llrp.ltk.types.UnsignedLong_DATETIME;
import org.llrp.ltk.types.UnsignedShort;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import au.com.bytecode.opencsv.CSVReader;

@Parameters(separators = "=")
public class LlrpReader extends LLRPConnection implements Runnable {

    public static final int DEFAULT_LLRP_PORT = 5084;
    public static final int DEFAULT_SSH_PORT = 2222;
    public static final int DEFAULT_TELNET_PORT = 2323;
    
    @Parameter(names = { "--port", "-p" }, description = "LLRP port")
    protected int port;
    @Parameter(names = { "--sshPort", "--sshport", "-sp" }, description = "SSH port")
    protected int sshPort;
    @Parameter(names = { "--telnetPort", "--telnetport", "-tp" }, description = "Telnet port")
    protected int telnetPort;
    @Parameter(names = { "--reportFile", "--reportfile", "-f" }, description = "Report file")
    protected String reportFile;
    @Parameter(names = { "--non-interactive", "-ni" }, description = "Non-interactive mode")
    protected boolean noninteractive = false;
    @Parameter(names = { "--skip-starts", "-s" }, description = "Do not report starts")
    protected boolean skipStarts = false;
    @Parameter(names = { "--offset", "-o" }, description = "Start offset in seconds")
    protected int offset;
    
    
    protected final Random rn = new Random();
    protected long started = 0;
    
    protected int messageId = 1;
    
    protected SshServer sshServer;
    protected TelnetServer telnetServer;
    
    protected ServerSocket serverSocket = null;
    protected Socket clientSocket = null;
    protected Thread listener = null;
    
    protected Map<Long, Integer> reports = new TreeMap<Long, Integer>();
    
    protected Map<Socket, Thread> threads = new HashMap<Socket, Thread>();

    public LlrpReader() {
        this.port = DEFAULT_LLRP_PORT;
        this.telnetPort = DEFAULT_TELNET_PORT;
        this.sshPort = DEFAULT_SSH_PORT;
        this.offset = 0;
    }
        
    public LlrpReader(int port) {
        this.port = port;
        this.offset = 0;
    }
        
    public LlrpReader(int port, int sshPort, int telnetPort) {
        this.port = port;
        this.sshPort = sshPort;
        this.telnetPort = telnetPort;
        this.offset = 0;
    }
        
    @Override
    public boolean reconnect() {
        return false;
    }
    
    public int getPort() {
        return port;
    }

    public int getSshPort() {
    	return sshPort;
    }

    public int getTelnetPort() {
		return telnetPort;
	}
    
    public void loadReports() throws IOException {
        loadReports(new FileReader(reportFile));
    }

    public void loadReports(final Reader rdr) throws IOException {
        CSVReader reader = new CSVReader(rdr);
        List<String[]> rows = reader.readAll();
        reader.close();
        
        for (String[] fields : rows) {
            final Integer bib = Integer.parseInt(fields[0]);
            Long start = Long.parseLong(fields[7]);
            final Long duration = Long.parseLong(fields[8]);
            Long finish = start + duration;
            
            if (duration > 0) {
                if (!this.skipStarts) {
                    while (reports.containsKey(start)) {
                        start++;
                    }
                    reports.put(start, bib);
                }
                while (reports.containsKey(finish)) {
                    finish++;
                }
                reports.put(finish, bib);
            }
        }
        for (final Entry<Long, Integer> e: reports.entrySet()) {
            System.out.println(e.getKey() + ": " + e.getValue());
        }
    }

    public void bind() throws IOException {

        int tries = 0;
        while (tries < 10) {
            try {
                serverSocket = new ServerSocket(port);
                break;
            } catch (Exception e) {
                tries++;
                try {
                    System.out.println("Unable to bind to port " + port + " try: " + tries);
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                }
            }
        }
        if (tries >= 10) {
            throw new IOException("Unable to start LLRP listener on port " + port);
        }
        listener = new Thread(this);
        listener.start();

        sshServer = SshServer.setUpDefaultServer();
        sshServer.setKeyPairProvider(new SimpleGeneratorHostKeyProvider());
        sshServer.setPort(sshPort);
        Factory<Command> shellFactory = new Factory<Command>() {

			@Override
			public Command create() {
				return new SshServerThread();
			}
        	
        };
		sshServer.setShellFactory(shellFactory);
		sshServer.setPasswordAuthenticator(new PasswordAuthenticator() {

	        @Override
	        public boolean authenticate(String u, String p, ServerSession s) {
	            return ("root".equals(u) && "impinj".equals(p));
	        }
	    });
        
        sshServer.start();
        
        try {
            telnetServer = new TelnetServer(telnetPort);
        } catch (Exception e) {
            System.out.println("caught exception trying to start telnetServer on port " + telnetPort);
            throw new IOException("caught exception trying to start telnetServer on port " + telnetPort);
        }

    }

    public void disconnect() {
        if (clientSocket != null) {
            synchronized (clientSocket) {
                try {
                    clientSocket.notify();
                } catch (Exception e) {
                    System.err.println("Exception in notify, " + e.getMessage());
                }
            }
        }
    }

    public void stop() {
        if (telnetServer != null) {
            telnetServer.stop();
        }
        
        if (sshServer != null) {
            try {
                sshServer.stop();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        listener.interrupt();
        
        // Closing the server socket will cause the run loop to be interrupted
        try {
            serverSocket.close();
            System.out.println("LlrpReader::stop - server socket has been closed");
        } catch (Exception e) {
            System.err.println("LlrpReader::stop - exception in server socket close, " + e.getMessage());
        }
    }
    
    @Override
    public void run() {
        boolean error = false;
        while (!error) {
            try {
                clientSocket = serverSocket.accept();
                System.out.println("llrp socket connection [" + clientSocket.getInetAddress() + "]");
                final Thread thread = new Thread(new LlrpWorker(clientSocket));
                threads.put(clientSocket, thread);
                thread.start();
            } catch (IOException e) {
                error = true;
                if (e instanceof SocketException && "Socket closed".equals(e.getMessage())) {
                    System.err.println("LlrpReader::run - server socket was closed");
                } else {
                    System.err.println("LlrpReader::run - exception in server socket accept, " + e.getMessage());
                }
                
                // Stop all threads
                for (Entry<Socket, Thread> thread : threads.entrySet()) {
                    thread.getValue().interrupt();
                }
            }
        }

        try {
            if (!serverSocket.isClosed()) {
                serverSocket.close();
                System.out.println("LlrpReader::run - server socket has been closed");
            }
        } catch (Exception e) {
            System.err.println("LlrpReader::run - exception in server socket close, " + e.getMessage());
        }
    }

    class LlrpWorker implements Runnable {
        
        private final Socket clientSocket;
        
        public LlrpWorker(final Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            handleLlrpConnection(clientSocket);
        }
        
    }

    private void handleLlrpConnection(final Socket clientSocket) {
        boolean receivedCloseConnection = false;
        try {
            long lastCheck = System.currentTimeMillis();
            final DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            final DataInputStream in = new DataInputStream(clientSocket.getInputStream());

            out.write(makeConnectMessage().encodeBinary());

            final ByteStreamHelper helper = new ByteStreamHelper(in);
            
            boolean interrupted = false;
            while (!interrupted) {
                byte[] bytes = helper.getBytes();
                if (bytes != null) {
                    LLRPMessage incoming;
                    try {
                        incoming = LLRPMessageFactory.createLLRPMessage(bytes);
                    } catch (InvalidLLRPMessageException e) {
                        System.err.println("Exception createLLRPMessage: " + e.getMessage());
                        for (byte b : bytes) {
                            System.out.print(Integer.toHexString(b) + " ");
                        }
                        System.out.println();
                        continue;
                    }
                    if (incoming instanceof CLOSE_CONNECTION) {
                        receivedCloseConnection = true;
                    }
                    LLRPMessage response = messageReceived(incoming);
                    if (response != null) {
                        response.setMessageID(new UnsignedInteger(messageId++));
                        out.write(response.encodeBinary());
                        System.out.println("[" + Thread.currentThread().getId() + "] --> " + response.getName() + " (" + response.getMessageID().toString() + ")");
                        lastCheck = System.currentTimeMillis();
                    }
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        System.err.println("Exception in wait, " + e.getMessage());
                        interrupted = true;
                    }
                    if (System.currentTimeMillis() - lastCheck > 30000) {
                        out.write(new KEEPALIVE().encodeBinary());
                        out.flush();
                        lastCheck = System.currentTimeMillis(); 
                    }
                }
                
                if (Thread.interrupted()) {
                    interrupted = true;
                    System.err.println("we were unterrupted");
                }
            }
        } catch (Exception e) {
        	if (!receivedCloseConnection && !(e instanceof InterruptedException)) {
                System.err.println("Exception in while loop, " + e.getMessage());
                e.printStackTrace();
        	}
        }
        try {
            clientSocket.close();
            System.err.println("handleLlrpConnection: client socket has been closed");
        } catch (Exception e) {
            if (!receivedCloseConnection) {
                System.err.println("handleLlrpConnection: exception in client socket close, " + e.getMessage());
            }
        }
    }

	public LLRPMessage messageReceived(final LLRPMessage message) throws InvalidLLRPMessageException, JDOMException, IOException {
        LLRPMessage response = null;
        System.out.println("<-- " + message.getName() + " (" + message.getMessageID().toString() + ")");
        if (message instanceof GET_READER_CAPABILITIES) {
            response = makeReaderCapabilitiesResponse();
        } else if (message instanceof SET_READER_CONFIG) {
            response = new SET_READER_CONFIG_RESPONSE();
            ((SET_READER_CONFIG_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        } else if (message instanceof GET_READER_CONFIG) {
            GET_READER_CONFIG_RESPONSE config = new GET_READER_CONFIG_RESPONSE();
            Identification identification = new Identification();
            IdentificationType idtype = new IdentificationType(IdentificationType.EPC);
            identification.setIDType(idtype);
            UnsignedByteArray_HEX readerID = new UnsignedByteArray_HEX("01234567");
            identification.setReaderID(readerID);
            for (int i = 0; i < 4; i++) {
                AntennaProperties antennaProperties = new AntennaProperties();
                antennaProperties.setAntennaConnected(new Bit(true));
                antennaProperties.setAntennaGain(new SignedShort(10));
                antennaProperties.setAntennaID(new UnsignedShort(i));
                config.addToAntennaPropertiesList(antennaProperties);
            }
            config.setIdentification(identification);
            config.setLLRPStatus(makeSuccessStatus());
            
            response = config;
        } else if (message instanceof ADD_ROSPEC) {
            response = new ADD_ROSPEC_RESPONSE();
            ((ADD_ROSPEC_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        } else if (message instanceof ENABLE_ROSPEC) {
            response = new ENABLE_ROSPEC_RESPONSE();
            ((ENABLE_ROSPEC_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        } else if (message instanceof DELETE_ROSPEC) {
            response = new DELETE_ROSPEC_RESPONSE();
            ((DELETE_ROSPEC_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        } else if (message instanceof DELETE_ACCESSSPEC) {
            response = new DELETE_ACCESSSPEC_RESPONSE();
            ((DELETE_ACCESSSPEC_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        } else if (message instanceof START_ROSPEC) {
            if (started == 0) {
                started = System.currentTimeMillis() - offset;
            }
            response = new START_ROSPEC_RESPONSE();
            ((START_ROSPEC_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        } else if (message instanceof STOP_ROSPEC) {
            response = new STOP_ROSPEC_RESPONSE();
            ((STOP_ROSPEC_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        } else if (message instanceof GET_REPORT) {
            RO_ACCESS_REPORT report = new RO_ACCESS_REPORT();
            long now = System.currentTimeMillis();
            synchronized (reports) {
                final Iterator<Long> times = reports.keySet().iterator();
                while (times.hasNext()) {
                    final Long time = times.next();
                    if (started + time <= now) {
                        TagReportData tagReportData = new TagReportData();
                        
                        String tag = reports.get(time).toString();
                        if (tag.length() % 2 > 0) {
                            tag = "0" + tag;
                        }
        
                        // EPC
                        EPCData epcData = new EPCData();
                        BitArray_HEX epc = new BitArray_HEX(tag);
                        epcData.setEPC(epc);
                        tagReportData.setEPCParameter(epcData);
                        
                        // antenna
                        AntennaID antennaID = new AntennaID();
                        antennaID.setAntennaID(new UnsignedShort(rn.nextInt(4)));
                        tagReportData.setAntennaID(antennaID);
                        
                        // rssi
                        PeakRSSI peakRSSI = new PeakRSSI();
                        peakRSSI.setPeakRSSI(new SignedByte(50));
                        tagReportData.setPeakRSSI(peakRSSI);
                        
                        // seen
                        UTCTimestamp timestamp = new UTCTimestamp();
                        UnsignedLong_DATETIME micro = new UnsignedLong_DATETIME((started + time)*1000);
                        timestamp.setMicroseconds(micro);
                        FirstSeenTimestampUTC firstSeenTimestampUTC = new FirstSeenTimestampUTC();
                        firstSeenTimestampUTC.setMicroseconds(timestamp.getMicroseconds());
                        tagReportData.setFirstSeenTimestampUTC(firstSeenTimestampUTC);
                        LastSeenTimestampUTC lastSeenTimestampUTC = new LastSeenTimestampUTC();
                        lastSeenTimestampUTC.setMicroseconds(timestamp.getMicroseconds());
                        tagReportData.setLastSeenTimestampUTC(lastSeenTimestampUTC);
                        
                        report.addToTagReportDataList(tagReportData);
                        
                        times.remove();
                    }
                }
            }
            
            response = report;
        } else if (message instanceof CLOSE_CONNECTION) {
            response = new CLOSE_CONNECTION_RESPONSE();
            ((CLOSE_CONNECTION_RESPONSE)response).setLLRPStatus(makeSuccessStatus());
        }
        
        return response;
    }

    public LLRPStatus makeSuccessStatus() {
        LLRPStatus status = new LLRPStatus();
        status.setStatusCode(new StatusCode(StatusCode.M_Success));
        UTF8String_UTF_8 errorDescription = new UTF8String_UTF_8("");
        status.setErrorDescription(errorDescription);

        return status;
    }
    
    public LLRPMessage makeConnectMessage() {
        LLRPMessage msg = new READER_EVENT_NOTIFICATION();
        
        BitList version = new BitList(1, 0, 1);
        msg.setVersion(version);
        ReaderEventNotificationData readerEventNotificationData = new ReaderEventNotificationData();
        UTCTimestamp timestamp = new UTCTimestamp();
        UnsignedLong_DATETIME micro = new UnsignedLong_DATETIME(System.currentTimeMillis()*1000);
        timestamp.setMicroseconds(micro);
        readerEventNotificationData.setTimestamp(timestamp);
        ConnectionAttemptEvent connectionAttemptEvent = new ConnectionAttemptEvent(); 
        connectionAttemptEvent.setStatus(new ConnectionAttemptStatusType(ConnectionAttemptStatusType.Success));
        readerEventNotificationData.setConnectionAttemptEvent(connectionAttemptEvent);
        ((READER_EVENT_NOTIFICATION)msg).setReaderEventNotificationData(readerEventNotificationData);

        return msg;
    }
    
    public LLRPMessage makeReaderCapabilitiesResponse() throws InvalidLLRPMessageException, JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();

        GET_READER_CAPABILITIES_RESPONSE response = new GET_READER_CAPABILITIES_RESPONSE();
        
        URL url = this.getClass().getResource("/reader_capabilities_response.xml");
        Document xml = builder.build(url);
        response.decodeXML(xml);

        return response;
    }
    
    public static void main(String[] args) throws IOException {
        LlrpReader reader = new LlrpReader();
        
        new JCommander(reader, args);
        
        reader.loadReports();
        reader.bind();

        System.out.println("  _     _     ____  ____    ____                _           ");
        System.out.println(" | |   | |   |  _ \\|  _ \\  |  _ \\ ___  __ _  __| | ___ _ __ ");
        System.out.println(" | |   | |   | |_) | |_) | | |_) / _ \\/ _` |/ _` |/ _ \\ '__|");
        System.out.println(" | |___| |___|  _ <|  __/  |  _ <  __/ (_| | (_| |  __/ |   ");
        System.out.println(" |_____|_____|_| \\_\\_|     |_| \\_\\___|\\__,_|\\__,_|\\___|_|");            

        if (System.getenv("OFFSET") != null) {
            reader.offset = Integer.parseInt(System.getenv("OFFSET"));
        }

        if (System.getenv("SKIP_STARTS") != null) {
            reader.skipStarts = true;
        }

        if (System.getenv("LLRPREADER_NI") != null) {
            reader.noninteractive = true;
        }

        if (reader.noninteractive) {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        } else {
            String eof = "d";
            if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
                eof = "z";
            }

            System.out.println("LLRP Reader is running, type CTRL-" + eof + " to exit");
            System.out.println("Loaded " + reader.reports.size() + " reports");

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            while (in.readLine() != null) {
            }
        }

        System.out.println("Exiting...");
        reader.stop();
    }
}
