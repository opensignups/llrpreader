package com.opensignups.timing.llrpreader;

public class ImpinjCommandProcessor {

    public static StringBuffer process(final String str) {

        final StringBuffer sb = new StringBuffer();

        if (str.startsWith("config system time ")) {
            String time = str.substring(19).replace("\r\n", "");
            // yyyy.MM.dd-HH:mm:ss
            if (time.matches("^(\\d{4}\\.\\d{2}\\.\\d{2}-\\d{2}:\\d{2}:\\d{2})$")) {
                sb.append("Status='0,Success'\r\n> ");
            } else {
                sb.append("Status='1,Error'\r\n> ");
            }
        } else if (str.startsWith("show system platform")) {
            sb.append("Status='0,Success'\r\n");
            sb.append("BootEnvVersion='1'\r\n");
            sb.append("HLAVersion='332-001-000'\r\n");
            sb.append("HardwareVersion='250-004-000'\r\n");
            sb.append("IntHardwareVersion='250-004-000'\r\n");
            sb.append("ModelName='Speedway R420'\r\n");
            sb.append("SerialNumber='370-11-22-3344'\r\n");
            sb.append("IntSerialNumber='370-11-22-3344'\r\n");
            sb.append("MACAddress='00:16:25:10:20:30'\r\n");
            sb.append("FeaturesValid='1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32'\r\n");
            sb.append("BIOSVersion='1.0.5.240'\r\n");
            sb.append("PTN='001.002'\r\n");
            sb.append("UptimeSeconds='88559'\r\n");
            sb.append("BootStatus='0'\r\n");
            sb.append("BootReason='Cold'\r\n");
            sb.append("PowerFailTime='4294967295'\r\n");
            sb.append("ActivePowerSource='poe'\r\n");
            sb.append("> ");
        } else if (str.startsWith("show system summary")) {
            sb.append("Status='0,Success'\r\n");
            sb.append("SysDesc='Speedway R420'\r\n");
            sb.append("SerialNumber='370-11-22-3344'\r\n");
            sb.append("MACAddress='00:16:25:10:20:30'\r\n");
            sb.append("> ");
        } else if (str.startsWith("exit")) {
            return null;
        } else {
            sb.append("\r\n> ");
        }

        return sb;
    }
}
