package com.opensignups.timing.llrpreader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.apache.sshd.server.Command;
import org.apache.sshd.server.Environment;
import org.apache.sshd.server.ExitCallback;

public class SshServerThread implements Command, Runnable {

    Environment env;
    InputStream in;
    OutputStream out;

    Thread thread;

    @Override
    public void destroy() throws Exception {
    }

    @Override
    public void start(Environment env) throws IOException {
        // must start new thread to free up the input stream
        this.env = env;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            out.write(" > ".getBytes());
            out.flush();
            String line = reader.readLine();
            while (line != null) {
                final StringBuffer sb = ImpinjCommandProcessor.process(line);
                if (sb == null) {
                    break;
                }
                out.write(sb.toString().getBytes());
                out.flush();
                line = reader.readLine();
            }
        } catch (IOException e) {
        }
    }

    @Override
    public void setErrorStream(OutputStream err) {
        // noop
    }

    @Override
    public void setExitCallback(ExitCallback ec) {
        // noop
    }

    @Override
    public void setInputStream(InputStream in) {
        this.in = in;
    }

    @Override
    public void setOutputStream(OutputStream out) {
        this.out = out;
    }

}
