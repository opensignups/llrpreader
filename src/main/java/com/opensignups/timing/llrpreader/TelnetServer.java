package com.opensignups.timing.llrpreader;

/*
 * Copyright 2003-2004 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/***
 * Simple TCP server. Waits for connections on a TCP port in a separate thread.
 * <p>
 * 
 * @author Bruno D'Avanzo
 ***/
public class TelnetServer implements Runnable {
    ServerSocket serverSocket = null;
    Socket clientSocket = null;
    Thread listener = null;

    /***
     * test of client-driven subnegotiation.
     * <p>
     * 
     * @param port
     *            - server port on which to listen.
     ***/
    public TelnetServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);

        listener = new Thread(this);

        listener.start();
    }

    /***
     * Run for the thread. Waits for new connections
     ***/
    public void run() {
        boolean bError = false;
        while (!bError) {
            try {
                clientSocket = serverSocket.accept();
                System.out.println("telnet socket connection [" + clientSocket.getInetAddress() + "]");
                synchronized (clientSocket) {
                    try {
                        DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
                        DataInputStream in = new DataInputStream(clientSocket.getInputStream());

                        out.writeBytes("login:");
                        while (true) {
                            int available = in.available();
                            if (available > 0) {
                                byte[] bytes = new byte[available];
                                in.read(bytes);

                                String str = new String(bytes);
                                if (str.endsWith("root\r\n")) {
                                    out.writeBytes("Password:");
                                } else if ("impinj\r\n".equals(str)) {
                                    out.writeBytes(">");
                                } else {
                                    final StringBuffer sb = ImpinjCommandProcessor.process(str);
                                    if (sb == null) {
                                        break;
                                    } else {
                                        out.writeBytes(sb.toString());
                                    }
                                }
                            } else {
                                Thread.sleep(100);
                            }

                        }
                    } catch (Exception e) {
                        if (!(e instanceof InterruptedException)) {
                            System.err.println("Exception in wait, " + e.getMessage());
                        }
                    }
                    try {
                        clientSocket.close();
                    } catch (Exception e) {
                        System.err.println("Exception in close, " + e.getMessage());
                    }
                }
            } catch (IOException e) {
                bError = true;
            }
        }

        try {
            serverSocket.close();
            System.out.println("telnet socket connection closed");
        } catch (Exception e) {
            System.err.println("Exception in close, " + e.getMessage());
        }
    }

    /***
     * Disconnects the client socket
     ***/
    public void disconnect() {
        synchronized (clientSocket) {
            try {
                clientSocket.notify();
            } catch (Exception e) {
                System.err.println("Exception in notify, " + e.getMessage());
            }
        }
    }

    /***
     * Stop the listener thread
     ***/
    public void stop() {
        listener.interrupt();
        try {
            serverSocket.close();
        } catch (Exception e) {
            System.err.println("TelnetServer::stop - exception in server socket close, " + e.getMessage());
        }
    }

    /***
     * Gets the input stream for the client socket
     ***/
    public InputStream getInputStream() throws IOException {
        if (clientSocket != null) {
            return (clientSocket.getInputStream());
        } else {
            return (null);
        }
    }

    /***
     * Gets the output stream for the client socket
     ***/
    public OutputStream getOutputStream() throws IOException {
        if (clientSocket != null) {
            return (clientSocket.getOutputStream());
        } else {
            return (null);
        }
    }
}
